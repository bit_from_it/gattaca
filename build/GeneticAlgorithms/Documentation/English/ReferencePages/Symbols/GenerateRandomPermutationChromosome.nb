(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      9120,        291]
NotebookOptionsPosition[      6407,        206]
NotebookOutlinePosition[      8185,        256]
CellTagsIndexPosition[      8101,        251]
WindowTitle->GenerateRandomPermutationChromosome - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:GeneticAlgorithms/guide/GeneticAlgorithms"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["GENETICALGORITHMS PACLET SYMBOL", "PacletNameCell"], Cell[
    TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"More About \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"Genetic Algorithms\"\>":>
       Documentation`HelpLookup["paclet:GeneticAlgorithms/guide/Main"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->
         False], {"\<\"GeneticAlgorithms/ref/\
GenerateRandomPermutationChromosome\"\>":>
       None, "\<\"Copy Mathematica url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
             "GeneticAlgorithms/ref/GenerateRandomPermutationChromosome"]}, 
            Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; Null], 
       Delimiter, "\<\"Copy web url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
              BoxData[
               MakeBoxes[
                Hyperlink[
                "http://reference.wolfram.com/mathematica/GeneticAlgorithms/\
ref/GenerateRandomPermutationChromosome.html"], StandardForm]], "Input", 
              TextClipboardType -> "PlainText"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; 
        Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
         NotebookLocate[{
           URL[
            StringJoin[
            "http://reference.wolfram.com/mathematica/", 
             "GeneticAlgorithms/ref/GenerateRandomPermutationChromosome", 
             ".html"]], None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}]
    }], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell["GenerateRandomPermutationChromosome", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"GenerateRandomPermutationChromosome", "[", 
       StyleBox["size", "TI"], "]"}]], "InlineFormula"],
     " \[LineSeparator]Generates a chromosome specifically designed to deal \
with permutations problems with a definite ",
     Cell[BoxData[
      StyleBox["size", "TI"]], "InlineFormula"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->537255402],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->1630289426],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GenericAlgorithms`\.1d\>\"", "]"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->1730231303],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomPermutationChromosome", "[", "10", "]"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->376340412],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "9", ",", "3", ",", "10", ",", "2", ",", "4", ",", "1", ",", "5", ",", "7", 
   ",", "6", ",", "8"}], "}"}]], "Output",
 ImageSize->{194, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->925814444]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomPermutationChromosome", "[", "5", "]"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->823276293],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"3", ",", "4", ",", "1", ",", "2", ",", "5"}], "}"}]], "Output",
 ImageSize->{97, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->1644337793]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["MORE ABOUT",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["Genetic Algorithms",
 BaseStyle->"Link",
 ButtonData->"paclet:GeneticAlgorithms/guide/Main"]], "MoreAbout",
 CellID->530530709]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"GenerateRandomPermutationChromosome - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2013, 12, 18, 7, 22, 29.647780}", "context" -> 
    "GeneticAlgorithms`", "keywords" -> {}, "index" -> True, "label" -> 
    "GeneticAlgorithms Paclet Symbol", "language" -> "en", "paclet" -> 
    "GeneticAlgorithms", "status" -> "None", "summary" -> 
    "GenerateRandomPermutationChromosome[size] Generates a chromosome \
specifically designed to deal with permutations problems with a definite \
size.", "synonyms" -> {}, "title" -> "GenerateRandomPermutationChromosome", 
    "type" -> "Symbol", "uri" -> 
    "GeneticAlgorithms/ref/GenerateRandomPermutationChromosome"}, 
  "LinkTrails" -> "", "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> 
  "9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[4104, 115, 461, 13, 70, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->537255402]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 7958, 244}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[628, 21, 76, 1, 70, "SymbolColorBar"],
Cell[707, 24, 200, 5, 70, "LinkTrail"],
Cell[910, 31, 2521, 59, 70, "AnchorBarGrid",
 CellID->1],
Cell[3434, 92, 78, 1, 70, "ObjectName",
 CellID->1224892054],
Cell[3515, 95, 564, 16, 70, "Usage",
 CellID->982511436],
Cell[CellGroupData[{
Cell[4104, 115, 461, 13, 70, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->537255402],
Cell[CellGroupData[{
Cell[4590, 132, 149, 5, 70, "ExampleSection",
 CellID->1630289426],
Cell[4742, 139, 131, 3, 70, "Input",
 CellID->1730231303],
Cell[CellGroupData[{
Cell[4898, 146, 132, 3, 70, "Input",
 CellID->376340412],
Cell[5033, 151, 292, 9, 34, "Output",
 CellID->925814444]
}, Open  ]],
Cell[CellGroupData[{
Cell[5362, 165, 131, 3, 70, "Input",
 CellID->823276293],
Cell[5496, 170, 234, 7, 34, "Output",
 CellID->1644337793]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5791, 184, 419, 12, 70, "MoreAboutSection",
 CellID->38303248],
Cell[6213, 198, 152, 3, 70, "MoreAbout",
 CellID->530530709]
}, Open  ]],
Cell[6380, 204, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

