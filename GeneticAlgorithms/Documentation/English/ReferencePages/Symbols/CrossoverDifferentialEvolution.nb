(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     11307,        463]
NotebookOptionsPosition[      7062,        311]
NotebookOutlinePosition[      7691,        336]
CellTagsIndexPosition[      7613,        331]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/CrossoverDifferentialEvolution", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["CrossoverDifferentialEvolution", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"CrossoverDifferentialEvolution", "[", 
   RowBox[{
    StyleBox["originalVector", "TI"], ",", 
    StyleBox["mutatedVector", "TI"], ",", 
    StyleBox["crossoverConstant", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]CrossoverDifferentialEvolution applies a crossover \
operation between an ",
 Cell[BoxData[
  StyleBox["originalVector", "TI"]], "InlineFormula"],
 " and a ",
 Cell[BoxData[
  StyleBox["mutatedVector", "TI"]], "InlineFormula"],
 " with a given ",
 Cell[BoxData[
  StyleBox["crossoverConstant", "TI"]], "InlineFormula"],
 " for a differential evolution algorithm."
}], "Usage",
 CellChangeTimes->{{3.562545434423875*^9, 3.562545548542324*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->582053094],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CrossoverDifferentialEvolution", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"6", ",", "7", ",", "8", ",", "9", ",", "0"}], "}"}], ",", ".5"}],
   "]"}]], "Input",
 CellChangeTimes->{{3.5625456591753273`*^9, 3.56254570346628*^9}},
 CellLabel->"In[8]:=",
 CellID->778338773],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"6", ",", "7", ",", "3", ",", "9", ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{{3.562545682743232*^9, 3.562545714378502*^9}, 
   3.562545762326118*^9},
 CellLabel->"Out[8]=",
 CellID->1742764788]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{1016, 766},
WindowMargins->{{4, Automatic}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[5542, 253, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 7475, 324}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 97, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1377, 59, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1435, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1517, 68, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1585, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1682, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1767, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1851, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1970, 90, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2026, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2092, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2164, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2231, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2303, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2367, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2431, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2497, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2578, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2682, 132, 73, 1, 62, "ObjectName",
 CellID->1224892054],
Cell[2758, 135, 755, 21, 86, "Usage",
 CellID->982511436],
Cell[3516, 158, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3595, 164, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3655, 167, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3737, 173, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3823, 176, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3918, 182, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3986, 185, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4072, 191, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4130, 194, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4210, 200, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4270, 203, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4353, 209, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4712, 222, 126, 3, 23, "Input",
 CellID->582053094],
Cell[CellGroupData[{
Cell[4863, 229, 388, 10, 23, "Input",
 CellID->778338773],
Cell[5254, 241, 239, 6, 22, "Output",
 CellID->1742764788]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5542, 253, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[5645, 257, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[5773, 262, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[5946, 269, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[6076, 274, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[6209, 279, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[6354, 285, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[6488, 290, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[6633, 295, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[6771, 300, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[6914, 305, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
