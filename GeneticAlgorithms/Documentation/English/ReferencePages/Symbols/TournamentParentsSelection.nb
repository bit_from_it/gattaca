(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     11850,        481]
NotebookOptionsPosition[      7511,        325]
NotebookOutlinePosition[      8141,        350]
CellTagsIndexPosition[      8063,        345]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/TournamentParentsSelection", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["TournamentParentsSelection", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"TournamentParentsSelection", "[", 
   RowBox[{
    StyleBox["fitnessesArray", "TI"], ",", 
    StyleBox["roundSize", "TI"], ",", 
    StyleBox["numberOfParents", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]TournamentParentsSelection returns an array of size ",
 Cell[BoxData[
  StyleBox["numberOfParents", "TI"]], "InlineFormula"],
 " with the indexes of the selected parents for crossover operations \
according to the to the ",
 Cell[BoxData[
  StyleBox["fitnessesArray", "TI"]], "InlineFormula"],
 " with a given ",
 Cell[BoxData[
  StyleBox["roundSize", "TI"]], "InlineFormula"],
 " with the tournament parents selection algorithm."
}], "Usage",
 CellChangeTimes->{{3.5586452364221497`*^9, 3.5586453272358*^9}, {
  3.558645383182994*^9, 3.5586453953567944`*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TournamentParentsSelection", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"10", ",", "5", ",", "8", ",", "20", ",", "33"}], "}"}], ",", "3",
    ",", "5"}], "]"}]], "Input",
 CellChangeTimes->{{3.558645344677761*^9, 3.558645357461516*^9}},
 CellLabel->"In[11]:=",
 CellID->1443284231],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"5", ",", "1", ",", "5", ",", "5", ",", "3"}], "}"}]], "Output",
 CellChangeTimes->{{3.558645352603351*^9, 3.558645357886643*^9}},
 CellLabel->"Out[11]=",
 CellID->875423014]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TournamentParentsSelection", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "10", ",", "5", ",", "8", ",", "20", ",", "33", ",", "3", ",", "2", ",", 
     "4", ",", "1"}], "}"}], ",", "5", ",", "5"}], "]"}]], "Input",
 CellChangeTimes->{{3.5586453668959*^9, 3.558645371670142*^9}},
 CellLabel->"In[12]:=",
 CellID->824130701],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"4", ",", "4", ",", "4", ",", "5", ",", "3"}], "}"}]], "Output",
 CellChangeTimes->{3.558645372237998*^9},
 CellLabel->"Out[12]=",
 CellID->2042652204]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 160}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[5991, 267, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 7925, 338}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 93, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1373, 59, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1431, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1513, 68, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1581, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1678, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1763, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1847, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1966, 90, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2022, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2088, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2160, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2227, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2299, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2363, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2427, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2493, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2574, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2678, 132, 69, 1, 62, "ObjectName",
 CellID->1224892054],
Cell[2750, 135, 863, 22, 101, "Usage",
 CellID->982511436],
Cell[3616, 159, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3695, 165, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3755, 168, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3837, 174, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3923, 177, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[4018, 183, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[4086, 186, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4172, 192, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4230, 195, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4310, 201, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4370, 204, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4453, 210, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[CellGroupData[{
Cell[4834, 225, 309, 8, 23, "Input",
 CellID->1443284231],
Cell[5146, 235, 213, 5, 22, "Output",
 CellID->875423014]
}, Open  ]],
Cell[CellGroupData[{
Cell[5396, 245, 353, 9, 23, "Input",
 CellID->824130701],
Cell[5752, 256, 190, 5, 22, "Output",
 CellID->2042652204]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5991, 267, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[6094, 271, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[6222, 276, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[6395, 283, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[6525, 288, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[6658, 293, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[6803, 299, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[6937, 304, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[7082, 309, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[7220, 314, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[7363, 319, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
