(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     14747,        535]
NotebookOptionsPosition[     10497,        383]
NotebookOutlinePosition[     11127,        408]
CellTagsIndexPosition[     11048,        403]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/MutatePopulationCreep", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["MutatePopulationCreep", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"MutatePopulationCreep", "[", 
   RowBox[{
    StyleBox["population", "TI"], ",", 
    StyleBox["probability", "TI"], ",", 
    StyleBox["sigma", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]MutatePopulationCreep takes a ",
 Cell[BoxData[
  StyleBox["population", "TI"]], "InlineFormula"],
 " as an input and applies a creep mutation operation with a given ",
 Cell[BoxData[
  StyleBox["probability", "TI"]], "InlineFormula"],
 " per allele and a ",
 Cell[BoxData[
  StyleBox["sigma", "TI"]], "InlineFormula"],
 " value for standard deviation for a normal distribution."
}], "Usage",
 CellChangeTimes->{{3.563483687281987*^9, 3.563483773241029*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellID->35670910],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"thesia", "=", 
   RowBox[{"GenerateRandomRealPopulation", "[", 
    RowBox[{"10", ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"5", ",", "3"}], "}"}], ",", "10"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"MutatePopulationCreep", "[", 
  RowBox[{"thesia", ",", ".5", ",", ".2"}], "]"}]}], "Input",
 CellChangeTimes->{{3.563483803018467*^9, 3.563483834310217*^9}, {
  3.56348388566109*^9, 3.563483963815075*^9}},
 CellLabel->"In[19]:=",
 CellID->373046857],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "3.112853495614196`", ",", "4.679912335307108`", ",", 
     "4.2794668990177005`", ",", "3.663984923784163`", ",", 
     "4.600185654823466`", ",", "3.5060456011432195`", ",", 
     "3.316430936713369`", ",", "2.8835368018123892`", ",", 
     "3.1115434403498576`", ",", "3.6901144672752695`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "3.5893705457545892`", ",", "3.0493685372053942`", ",", 
     "4.18010543265302`", ",", "3.446488248651305`", ",", 
     "3.3405533843198683`", ",", "4.105334751917219`", ",", 
     "4.2519328747812235`", ",", "3.9979907536840087`", ",", 
     "4.264235249666262`", ",", "4.886983477377032`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "3.7254242873700263`", ",", "4.113222653449679`", ",", 
     "4.491192971934956`", ",", "3.0322985129284596`", ",", 
     "4.131231804196258`", ",", "3.9924874223292717`", ",", 
     "4.644814549582815`", ",", "4.569354506195929`", ",", 
     "3.3316108163923785`", ",", "4.446315482204849`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "4.4248107662778295`", ",", "4.260269216234804`", ",", 
     "4.325622432658218`", ",", "3.691822532258561`", ",", 
     "3.649818494914851`", ",", "4.2283926750134615`", ",", 
     "3.64704052226874`", ",", "3.261080636320818`", ",", 
     "4.524999689317846`", ",", "3.082644079715186`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "4.150803447218918`", ",", "3.9698424781699697`", ",", 
     "3.704801331544555`", ",", "3.479642603965992`", ",", 
     "3.7883307886843727`", ",", "3.7482543053020807`", ",", 
     "3.520843594721304`", ",", "4.065115806303817`", ",", 
     "4.270326103235881`", ",", "3.0155939732435404`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "3.6411514312559867`", ",", "4.378813140028`", ",", "4.507653789488315`", 
     ",", "3.6947746523118736`", ",", "2.8727874897403716`", ",", 
     "3.8763521948982995`", ",", "4.957499679575859`", ",", 
     "4.796863478070058`", ",", "3.435228844260939`", ",", 
     "3.8996187515417304`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "3.4158799348207487`", ",", "3.865796217151111`", ",", 
     "3.836360413344708`", ",", "4.5519550774757995`", ",", 
     "3.645492273593197`", ",", "3.4211598648319064`", ",", 
     "4.889895609154313`", ",", "3.635516172869238`", ",", 
     "3.1757293168275047`", ",", "3.552711594738874`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "4.080086789299988`", ",", "3.67204827162913`", ",", "3.144841506195623`",
      ",", "3.61361121663726`", ",", "3.1636838028023457`", ",", 
     "4.68248971290297`", ",", "3.1998381265197318`", ",", 
     "3.477993489980773`", ",", "3.9057748799205156`", ",", 
     "5.071648932183543`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "3.3378035729448676`", ",", "4.224266429986586`", ",", 
     "4.532634709026835`", ",", "4.6168990294491765`", ",", 
     "3.0657202701909547`", ",", "4.286722685706813`", ",", 
     "4.3545743890430915`", ",", "4.885380455822096`", ",", 
     "4.82033733909258`", ",", "3.571598611313776`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
    "4.31136971516426`", ",", "4.421526280989356`", ",", "3.427207241832471`",
      ",", "4.523928338438541`", ",", "4.444805083928854`", ",", 
     "4.1814990240794385`", ",", "4.470377368147431`", ",", 
     "4.359480783207289`", ",", "4.984235329283859`", ",", 
     "4.840095166183655`"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.563483906918639*^9, 3.5634839640441732`*^9}},
 CellLabel->"Out[20]=",
 CellID->195321804]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 20, \
2012)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[8977, 325, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 10910, 396}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 88, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1368, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1426, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1508, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1576, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1673, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1758, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1842, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1961, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[2017, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2083, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2155, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2222, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2294, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2358, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2422, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2488, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2569, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2673, 132, 64, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2740, 135, 744, 20, 91, "Usage",
 CellID->982511436],
Cell[3487, 157, 42, 1, 25, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3566, 163, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3626, 166, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3708, 172, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3794, 175, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3889, 181, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3957, 184, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4043, 190, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4101, 193, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4181, 199, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4241, 202, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4324, 208, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4683, 221, 102, 2, 25, "Input",
 CellID->35670910],
Cell[CellGroupData[{
Cell[4810, 227, 543, 15, 41, "Input",
 CellID->373046857],
Cell[5356, 244, 3572, 75, 234, "Output",
 CellID->195321804]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8977, 325, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[9080, 329, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[9208, 334, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[9381, 341, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[9511, 346, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[9644, 351, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[9789, 357, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[9923, 362, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[10068, 367, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[10206, 372, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[10349, 377, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
