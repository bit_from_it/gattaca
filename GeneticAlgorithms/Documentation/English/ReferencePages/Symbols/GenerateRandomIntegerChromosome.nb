(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     11085,        461]
NotebookOptionsPosition[      6842,        309]
NotebookOutlinePosition[      7470,        334]
CellTagsIndexPosition[      7392,        329]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/GenerateRandomIntegerChromosome", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["GenerateRandomIntegerChromosome", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"GenerateRandomIntegerChromosome", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      StyleBox["xmin", "TI"], ",", 
      StyleBox["xmax", "TI"]}], "}"}], ",", 
    StyleBox["size", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]GenerateRandomIntegerChromosome creates a chromosome with \
integer values between ",
 Cell[BoxData[
  StyleBox["xmin", "TI"]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  StyleBox["xmax", "TI"]], "InlineFormula"],
 " and with a length of ",
 Cell[BoxData[
  StyleBox["size", "TI"]], "InlineFormula"],
 "."
}], "Usage",
 CellChangeTimes->{{3.560608309113821*^9, 3.5606083884096003`*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellID->1719894786],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomIntegerChromosome", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", "400"}], "}"}], ",", "10"}], "]"}]], "Input",
 CellLabel->"In[56]:=",
 CellID->89415777],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "83", ",", "333", ",", "116", ",", "61", ",", "151", ",", "153", ",", "171",
    ",", "245", ",", "271", ",", "186"}], "}"}]], "Output",
 CellChangeTimes->{3.560608403681753*^9},
 CellLabel->"Out[56]=",
 CellID->1141779682]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[5322, 251, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 7254, 322}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 98, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1378, 59, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1436, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1518, 68, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1586, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1683, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1768, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1852, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1971, 90, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2027, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2093, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2165, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2232, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2304, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2368, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2432, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2498, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2579, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2683, 132, 74, 1, 62, "ObjectName",
 CellID->1224892054],
Cell[2760, 135, 715, 23, 86, "Usage",
 CellID->982511436],
Cell[3478, 160, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3557, 166, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3617, 169, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3699, 175, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3785, 178, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3880, 184, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3948, 187, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4034, 193, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4092, 196, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4172, 202, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4232, 205, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4315, 211, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4674, 224, 104, 2, 23, "Input",
 CellID->1719894786],
Cell[CellGroupData[{
Cell[4803, 230, 202, 6, 23, "Input",
 CellID->89415777],
Cell[5008, 238, 265, 7, 22, "Output",
 CellID->1141779682]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5322, 251, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[5425, 255, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[5553, 260, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[5726, 267, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[5856, 272, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[5989, 277, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[6134, 283, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[6268, 288, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[6413, 293, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[6551, 298, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[6694, 303, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
