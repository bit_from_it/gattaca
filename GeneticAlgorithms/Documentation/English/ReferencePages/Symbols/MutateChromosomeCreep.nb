(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     12730,        511]
NotebookOptionsPosition[      8271,        351]
NotebookOutlinePosition[      8901,        376]
CellTagsIndexPosition[      8823,        371]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/MutateChromosomeCreep", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["MutateChromosomeCreep", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"MutateChromosomeCreep", "[", 
   RowBox[{
    StyleBox["chromosome", "TI"], ",", 
    StyleBox["probability", "TI"], ",", 
    StyleBox["sigma", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]MutateChromosomeCreep takes a ",
 Cell[BoxData[
  StyleBox["chromosome", "TI"]], "InlineFormula"],
 " as an input and mutates it according to a certain ",
 Cell[BoxData[
  StyleBox["probability", "TI"]], "InlineFormula"],
 " by adding a number extracted from a normal distribution with a ",
 Cell[BoxData[
  StyleBox["sigma", "TI"]], "InlineFormula"],
 " disperison value."
}], "Usage",
 CellChangeTimes->{{3.5599364608373117`*^9, 3.5599365608150253`*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->1894174764],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MutateChromosomeCreep", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"10", ",", "10", ",", "10", ",", "10"}], "}"}], ",", ".5", ",", 
   "1"}], "]"}]], "Input",
 CellChangeTimes->{3.560093932111994*^9},
 CellLabel->"In[8]:=",
 CellID->2044865150],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "10", ",", "11.539846383811515`", ",", "10.305990075069518`", ",", 
   "9.268677108551715`"}], "}"}]], "Output",
 CellChangeTimes->{{3.560093918157116*^9, 3.560093938779043*^9}, 
   3.5625892425209227`*^9},
 CellLabel->"Out[8]=",
 CellID->1373310261]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"shepard", "=", 
  RowBox[{"GenerateRandomRealChromosome", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "1"}], ",", "1"}], "}"}], ",", "5"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"MutateChromosomeCreep", "[", 
  RowBox[{"shepard", ",", ".5", ",", ".001"}], "]"}]}], "Input",
 CellChangeTimes->{{3.559936629545726*^9, 3.559936705650744*^9}},
 CellLabel->"In[9]:=",
 CellID->1097675420],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.6656406773555332`", ",", 
   RowBox[{"-", "0.9221458691179625`"}], ",", 
   RowBox[{"-", "0.012661095030798375`"}], ",", 
   RowBox[{"-", "0.6867565702490377`"}], ",", 
   RowBox[{"-", "0.17236027171369006`"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.5599366833678083`*^9, 3.559936713838517*^9}, 
   3.562589243025311*^9},
 CellLabel->"Out[9]=",
 CellID->71318671],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.6669387149680798`", ",", 
   RowBox[{"-", "0.9221458691179625`"}], ",", 
   RowBox[{"-", "0.01178386991223575`"}], ",", 
   RowBox[{"-", "0.6867565702490377`"}], ",", 
   RowBox[{"-", "0.17134269141572206`"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.5599366833678083`*^9, 3.559936713838517*^9}, 
   3.5625892430274963`*^9},
 CellLabel->"Out[10]=",
 CellID->357028471]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{40, Automatic}, {-2, Automatic}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[6751, 293, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 8685, 364}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 88, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1368, 59, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1426, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1508, 68, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1576, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1673, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1758, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1842, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1961, 90, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2017, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2083, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2155, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2222, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2294, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2358, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2422, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2488, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2569, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2673, 132, 64, 1, 62, "ObjectName",
 CellID->1224892054],
Cell[2740, 135, 742, 20, 86, "Usage",
 CellID->982511436],
Cell[3485, 157, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3564, 163, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3624, 166, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3706, 172, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3792, 175, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3887, 181, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3955, 184, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4041, 190, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4099, 193, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4179, 199, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4239, 202, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4322, 208, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4681, 221, 127, 3, 23, "Input",
 CellID->1894174764],
Cell[CellGroupData[{
Cell[4833, 228, 271, 8, 23, "Input",
 CellID->2044865150],
Cell[5107, 238, 292, 8, 22, "Output",
 CellID->1373310261]
}, Open  ]],
Cell[CellGroupData[{
Cell[5436, 251, 441, 12, 39, "Input",
 CellID->1097675420],
Cell[5880, 265, 408, 10, 22, "Output",
 CellID->71318671],
Cell[6291, 277, 411, 10, 22, "Output",
 CellID->357028471]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6751, 293, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[6854, 297, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[6982, 302, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[7155, 309, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[7285, 314, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[7418, 319, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[7563, 325, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[7697, 330, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[7842, 335, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[7980, 340, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[8123, 345, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
