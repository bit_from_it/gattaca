(* Mathematica Package *)
(* Programmer: Hector Manuel Sanchez Castellanos*)
(* Created by the Wolfram Workbench Oct 1, 2012 *)

BeginPackage["GeneticAlgorithms`"]

AppendGenerationFitnessData::usage = ""
ReturnNFittest::usage = "Returns N individuals that are the most fit in a population."
SelectNRandomDifferentFrom::usage = "Returns N indexes that are different from the input index."
GenerateRandomBinaryChromosome::usage = "GenerateRandomBinaryChromosome[size] generates a binary chromosome (with values of 1 or 0) with a definite size."
GenerateRandomPermutationChromosome::usage = "GenerateRandomPermutationChromosome[size] generates a permutations chromosome."
GenerateRandomRealChromosome::usage = "GenerateRandomRealChromosome[{xmin,xmax}, size] generates a real chromosome within a range of values."
GenerateRandomIntegerChromosome::usage= "Generates a random chromosome with integer values."
GenerateRandomBinaryPopulation::usage = "GenerateRandomBinaryPopulation[populationSize, chromosomeSize] creates a binary population of chromosomes."
GenerateRandomPermutationPopulation::usage = "GenerateRandomPermutationPopulation[populationSize, chromosomeSize] creates a permutations population of chromosomes." 
GenerateRandomRealPopulation::usage = "GenerateRandomRealPopulation[populationSize, {{xmin,xmax},chromosomeSize}] creates a real values population of chromosomes." 
GenerateRandomIntegerPopulation::usage = "Generates a random integers population of chromosomes."
MutateBinaryChromosomeFlip::usage = "Mutates a binary chromosome by inverting its value (each allele has a probability to mutate as indicated in the input of the function)." 
MutateChromosomeSwap::usage = "Mutates a chromosome by swaping the value of two alleles (each allele has a probability to mutate as indicated in the input of the function)."
MutateChromosomeCreep::usage = "Mutates a real-valued chromosome by adding a random number from a normal distribution."
MutateChromosomeIntegerCreep::usate= "Mutates an integer value chromosome by adding a random number from a normal distribution rounded and scaled."
MutateBinaryPopulationFlip::usage = "Mutates a whole population with a flip function with a fixed mutation probability (only for binary chromosomes)." 
MutatePopulationSwap::usage = "Mutates a whole population with a swap function with a fixes mutation probability (works with any type of chromosome)." 
MutatePopulationCreep::usage = "Mutates a population with a creep operation."
MutatePopulationIntegerCreep::usage = "Mutates a population with a integer creep operation."
MutateDifferentialEvolution::usage = "Creates a new vector to be used in differential evolution."
CrossoverChromosomesOnePoint::usage = "Performs a one point crossover between two chromosomes."
CrossoverChromosomesUniform::usage = "Performs a uniform crossover between two chromosomes in which each allele has a defined probability to be crossed." 
CrossoverChromosomesEdgeRecombination::usage = "Performs an edge recombination suited for permutations problems." 
CrossoverChromosomesUniformArithmetic::usage = "Performs a real valued crossover with an aritmetic weighted mean on two alleles."
CrossoverChromosomesUniformIntegerArithmetic::usage = "Performs an integer valued crossover with a rounded weighted mean on two alleles."
CrossoverPopulationOnePoint::usage ="Applies a one point crossover operator to a population. The individuals lotery pool is determined by the indexes in the selectedIndexes list and the probability of any individual to be selected to be reproduced with the adjascent individual is given by the probability parameter." 
CrossoverPopulationUniform::usage ="Applies a uniform crossover to a population. The individuals lotery pool is determined by the indexes in the selectedIndexes list and the probability of any individual to be selected to be reproduced with the adjascent individual is given by the probability parameter."
CrossoverPopulationEdgeRecombination::usage = "Applies an edges crossover to a population. The individuals lotery pool is determined by the indexes in the selectedIndexes list and the probability of any individual to be selected to be reproduced with the adjascent individual is given by the probability parameter." 
CrossoverPopulationUniformArithmetic::usage = "Applies a uniform arithmetic crossover to the selected individuals of a population."
CrossoverPopulationUniformIntegerArithmetic::usage = "Applies a uniform integer arithmetic crossover to a population."
CrossoverDifferentialEvolution::usage = "Applies a differential evolution crossover to two vectors."
TournamentParentSelection::usage = "*Selects one parent from the population with the tournament selection methodology."
TournamentParentsSelection::usage = "Selects several parents from the population with the tournament selection methodology."
RouletteParentSelection::usage = "Selects one parent from the population with the roulette selection methodology."
RouletteParentsSelection::usage = "Selects several parents from the population with the roulette selection methodology." 
ChromosomeRandomSteepestSearch::usage = "Performs a random local search on an individual based on agressive mutations on the chromosome and selects the fittest of the alternatives."
PopulationRandomSteepestSearch::usage = "Performs a random local search on selected individuals of a population selecting the fittest of each individuals."
TrainWithSGA::usage = "Trains a population according to Holland's Simple Genetic Algorithm definition."
PreProcessData::usage = "Returns Lists containing the iterativeData processed in the form: {{maxMaxList,maxMeanList,maxMinList},{meanMaxList,meanMeanList,meanMinList},{maxMaxList,meanMaxList,minMinLeast}}"
PlotSingleRunGA::usage = "Plots the output data of a genetic algorithm that has been run once."
PlotIteratedGA::usage = "Plots the output data of a genetic algorithm that has been iterated several times."
 
Begin["`Private`"]

(*Helper Functions*)
ThresholdProbability[value_, threshold_] := 
	(*Checks if the given value is higher than the threshold*)
 	Module[{}, 
 		If[value <= threshold, 1, 0]
 	]
InvertBinaryValue[binaryValue_] :=
	(*Inverts the binary value of an allele*) 
	Module[{},
  		If[binaryValue == 0, 1, 0]
  	]
SwapAlleles[chromosome_, {a_, b_}] :=
	(*Changes places of the values of two alleles in a chromosome*) 
 	Module[{tempValue, tempChromosome},
  		tempChromosome = chromosome;
  		tempValue = tempChromosome[[a]];
  		tempChromosome[[a]] = tempChromosome[[b]];
  		tempChromosome[[b]] = tempValue;
  		tempChromosome
 	]
SwapAllelesWithAccumulator[chromosome_, swapChromosome_, index_] := 
	(*Allows the nested calling of the swap alleles function*)
 	Module[{newChromosome},
  		newChromosome = If[swapChromosome[[index]] == 1, 
  				SwapAlleles[chromosome, {index, RandomInteger[{1, Length[chromosome]}]}], 
    			chromosome
    		];
  		{newChromosome, swapChromosome, index + 1}
  ]
SwapAllelesCrossover[chromosomeA_, chromosomeB_, index_] :=
	(*Changes the places of two alleles between two given chromosomes*) 
 	Module[{tempAllele, tempA, tempB},
 	 	tempA = chromosomeA;
  		tempB = chromosomeB;
  		tempAllele = tempA[[index]];
  		tempA[[index]] = tempB[[index]];
  		tempB[[index]] = tempAllele;
  		{tempA, tempB}
  	]
SwapAllelesCrossoverWithAccumulator[chromosomeA_, chromosomeB_, swapChromosome_, index_] := Module[{newChromosomes},
  	newChromosomes = If[
    		swapChromosome[[index]] == 1,
    		SwapAllelesCrossover[chromosomeA, chromosomeB, index],
    		{chromosomeA, chromosomeB}
    	];
  	Append[Append[newChromosomes, swapChromosome], index + 1]
  ]
CreepChromosomeWithAccumulator[chromosome_, flipChromosome_, sigma_, index_] := 
	Module[{newChromosome, tempChromosome},
  		newChromosome = If[flipChromosome[[index]] == 1,
    		tempChromosome = chromosome; ReplacePart[tempChromosome, index -> tempChromosome[[index]] + RandomVariate[NormalDistribution[0, sigma]]],
    		chromosome
    	];
  		{newChromosome, flipChromosome, sigma, index + 1}
  	]
CrossoverUniformWithAccumulator[matingPool_, crossChromosome_, uniformityProbability_, index_] :=
 	Module[{ newIndividuals},
  		If[crossChromosome[[index]] == 1,
   			newIndividuals = CrossoverChromosomesUniform[matingPool[[index]][[1]], matingPool[[index]][[2]], uniformityProbability];
   				{ReplacePart[matingPool, index -> newIndividuals], crossChromosome,uniformityProbability, index + 1},
   			{matingPool, crossChromosome, uniformityProbability, index + 1}
   		]
  ]
MeanAllelesCrossover[chromosomeA_,chromosomeB_,alpha_,index_]:=
	(*Performs an arithmetic crossover operation on two alleles*)
	Module[{tempA,tempB},
		tempA = chromosomeA;
		tempB = chromosomeB;
		tempA[[index]] = alpha*chromosomeA[[index]] + (1-alpha)*chromosomeB[[index]]; 
		tempB[[index]] = (1-alpha)*chromosomeA[[index]] + alpha*chromosomeB[[index]];
		{tempA,tempB}
	]	
GetEdges[chromosome_] :=
	Module[{neighborsA, where, before, after},
				(*Obtains the path edges of a chromosome for the edge recombination crossover algorithm*)
  		neighborsA = Table[
    		where = Position[chromosome, i][[1]];
    		before = If[where[[1]] == 1, chromosome[[Length[chromosome]]],chromosome[[where - 1]][[1]]];
    		after = If[where[[1]] == Length[chromosome], chromosome[[1]],chromosome[[where + 1]][[1]]];
    		{before, after}
    	,{i, 1, Length[chromosome]}]
    ]
CountOnes[boolList_] := 
	Module[{},
  		Total[boolList]
	]
AppendGenerationFitnessData[dataArray_,fitnessesList_]:=
	Module[{},
		(*{maxFitness, meanFitness, minFitness}*)
		{
			Append[dataArray[[1]],Max[fitnessesList]],
			Append[dataArray[[2]],Mean[fitnessesList]],
			Append[dataArray[[3]],Min[fitnessesList]]
		}
	]
ReturnNFittest[fitnessesList_, returnN_] := 
 	Module[{mixedList, sortedList, selectedList, range},
  		range = Range[1, Length[fitnessesList]];
  		mixedList = Transpose[{fitnessesList, range}];
  		sortedList = Sort[mixedList, #1[[1]] > #2[[1]] &];
  		selectedList = Take[sortedList, returnN];
  		selectedList[[All, 2]]
  ]
SelectNRandomDifferentFrom[n_, population_, currentIndex_] := 
	Module[{pool},
		pool = Drop[Range[Length[population]], {currentIndex}];
		Take[RandomSample[pool], n]
]

(*Chromosome and Population Generation Functions*)
GenerateRandomBinaryChromosome[size_] := 
	Module[{},
		RandomInteger[{0, 1}, size]
  	]  		
GenerateRandomPermutationChromosome[size_] :=
 	Module[{},
		RandomSample[Range[size]]
  	]
GenerateRandomRealChromosome[{xmin_,xmax_},size_]:=
	Module[{},
		RandomReal[{xmin,xmax},size]
	]
GenerateRandomIntegerChromosome[{xmin_,xmax_},size_]:=
	Module[{},
		RandomInteger[{xmin,xmax},size]
	]
GenerateRandomBinaryPopulation[populationSize_, chromosomeSize_] := 
 	Module[{},
  		Table[GenerateRandomBinaryChromosome[chromosomeSize],{i, 1,populationSize}]
  	]
GenerateRandomPermutationPopulation[populationSize_, chromosomeSize_] := 
  	Module[{},
  		Table[GenerateRandomPermutationChromosome[chromosomeSize],{i, 1, populationSize}]
  	]
GenerateRandomRealPopulation[populationSize_, {{xmin_,xmax_},chromosomeSize_}] := 
  	Module[{},
  		Table[GenerateRandomRealChromosome[{xmin,xmax},chromosomeSize],{i, 1, populationSize}]
  	]
GenerateRandomIntegerPopulation[populationSize_, {{xmin_,xmax_},chromosomeSize_}] := 
  	Module[{},
  		Table[GenerateRandomIntegerChromosome[{xmin,xmax},chromosomeSize],{i, 1, populationSize}]
  	]

(*Mutation Functions*)
MutateBinaryChromosomeFlip[chromosome_, probability_] :=
 	Module[{randomRealsChromosome, flipChromosome, realProbability},
  		randomRealsChromosome = RandomReal[{0, 1}, Length[chromosome]];
  		realProbability = 1 - probability;
  		flipChromosome = Ceiling[Threshold[randomRealsChromosome, realProbability]];
  		BitAnd[chromosome + flipChromosome, 1]
  ]
MutateChromosomeSwap[chromosome_, probability_] := 
 	Module[{randomRealsChromosome, realProbability, flipChromosome},
		randomRealsChromosome = RandomReal[{0, 1}, Length[chromosome]];
		realProbability = 1 - probability;
		flipChromosome = Ceiling[Threshold[randomRealsChromosome, realProbability]];
		Nest[(Apply[SwapAllelesWithAccumulator[#1, #2, #3] &, #]) &, {chromosome, flipChromosome, 1}, Length[chromosome]][[1]]
 	]
MutateChromosomeCreep[chromosome_, probability_, sigma_] := 
 	Module[{randomRealsChromosome, realProbability, flipChromosome},
  		randomRealsChromosome = RandomReal[{0, 1}, Length[chromosome]];
  		realProbability = 1 - probability;
  		flipChromosome = Ceiling[Threshold[randomRealsChromosome, realProbability]];
  		Nest[(Apply[CreepChromosomeWithAccumulator[#1, #2, #3, #4] &, #]) &, {chromosome, flipChromosome, sigma, 1}, Length[chromosome]][[1]]
  	]
MutateChromosomeIntegerCreep[chromosome_, probability_, sigma_] := 
 	Module[{},
  		Round[MutateChromosomeCreep[chromosome, probability, sigma]]
  	]
MutateBinaryPopulationFlip[population_, probability_] := 
	Module[{}, 
  		Map[MutateBinaryChromosomeFlip[#, probability] &, {population}][[1]]
  	]
MutatePopulationSwap[population_, probability_] := 
	Module[{},
  		Map[MutateChromosomeSwap[#, probability] &, {population}][[1]]
 	]
MutatePopulationCreep[population_,probability_,sigma_]:=
	Module[{},
		Map[MutateChromosomeCreep[#, probability, sigma] &, {population}][[1]]
	]
MutatePopulationIntegerCreep[population_,probability_,sigma_,multiplier_]:=
	Module[{},
		Map[MutateChromosomeIntegerCreep[#, probability, sigma, multiplier] &, {population}][[1]]
	]
MutateDifferentialEvolution[indexes_, population_, mutationFactor_] :=
  Module[{}, 
  	population[[indexes[[1]]]] + mutationFactor*(population[[indexes[[2]]]] - population[[indexes[[3]]]])
  ]

(*Parents Selection*)
TournamentParentSelection[scoresArray_, roundSize_] := 
 	Module[{randomCompetingIndexes,randomCompetingScores,maxInBatch,maxIndexes},
		randomCompetingIndexes = RandomSample[Range[Length[scoresArray]], roundSize];
		randomCompetingScores = scoresArray[[randomCompetingIndexes]];
		maxInBatch = Ordering[randomCompetingScores, -1][[1]];
		maxIndexes = Position[scoresArray, randomCompetingScores[[maxInBatch]]];
		maxIndexes[[RandomInteger[{1, Length[maxIndexes]}]]][[1]]
  ]
TournamentParentsSelection[scoresArray_, roundSize_, numberOfParents_] := 
	Module[{},
  		Table[TournamentParentSelection[scoresArray, roundSize],{i, 1, numberOfParents}]
  	]
RouletteParentSelection[scoresArray_] := 
 	Module[{totalProbability, lowLimit, highLimit, 
   		randomNumber, tempIndex, message, tempIndexArray},
  		totalProbability = Total[scoresArray];

  		lowLimit = 0;
  		highLimit = 0;
  		randomNumber = RandomReal[{0, 1}];

  		tempIndexArray = Table[
    		highLimit = highLimit + scoresArray[[i]]/totalProbability;
    
    		tempIndex = If[lowLimit < randomNumber && randomNumber <= highLimit, i , 0];
    		message = {randomNumber, lowLimit // N, highLimit // N, tempIndex};
    		lowLimit = lowLimit + scoresArray[[i]]/totalProbability;
    		message
    	,{i, 1, Length[scoresArray]}];
  		Total[Table[tempIndexArray[[n]][[4]], {n, 1, Length[tempIndexArray]}]]
  ]
RouletteParentsSelection[scoresArray_, numberOfParents_] := 
 	Module[{},
  		Table[RouletteParentSelection[scoresArray], {i,1,numberOfParents}]
  	]

(*Crossover Functions*)
CrossoverChromosomesOnePoint[individualA_, individualB_] := 
 		Module[{randomCrossoverIndex, newIndividualA, newIndividualB},
	 		randomCrossoverIndex = RandomInteger[{1, Length[individualA] - 1}];
 			newIndividualA = Flatten[Append[Take[individualA, randomCrossoverIndex],Take[individualB, randomCrossoverIndex - Length[individualA]]]];
  			newIndividualB = Flatten[Append[Take[individualB, randomCrossoverIndex],Take[individualA, randomCrossoverIndex - Length[individualA]]]];
			{newIndividualA, newIndividualB}
  		]
CrossoverChromosomesUniform[individualA_, individualB_, probability_] := 
 		Module[{randomRealsChromosome, realProbability, flipChromosome},
 			randomRealsChromosome = RandomReal[{0, 1}, Length[individualA]];
 			realProbability = 1 - probability;
 			flipChromosome = Ceiling[Threshold[randomRealsChromosome, realProbability]];
 			Nest[(Apply[SwapAllelesCrossoverWithAccumulator[#1, #2, #3, #4] &, #]) &,{individualA, individualB, flipChromosome, 1},Length[individualA]][[{1, 2}]]
  		]
CrossoverChromosomesEdgeRecombination[chromosomeA_, chromosomeB_] := 
 		Module[{edgesA,edgesB,merged,mergedNotRepeated,max,final,iteration,index,matches,currentSet,sizesInSet,minimumInNeighbors,lengthIndex,positionOfMinimumInSet,fullElementsList,missing,individual},
  			edgesA = GetEdges[chromosomeA];
  			edgesB = GetEdges[chromosomeB];
  				
  			merged = Join[edgesA, edgesB, 2];
  			mergedNotRepeated = Table[DeleteDuplicates[merged[[i]]], {i, 1, Length[merged]}];
  				 
  			max = Length[chromosomeA];
  			final = If[RandomInteger[{1, 2}] == 1, {chromosomeA[[1]]}, {chromosomeB[[1]]}];
  				
  			iteration = 1;
  			index = final[[iteration]];
  
  			individual = Table[
     			matches = Position[mergedNotRepeated, index];
     			mergedNotRepeated = Delete[mergedNotRepeated, matches];
     			currentSet = mergedNotRepeated[[index]];
    		 	sizesInSet = Table[
       				lengthIndex = currentSet[[i]];
       				Length[mergedNotRepeated[[lengthIndex]]]
       				,{i, 1, Length[currentSet]}
       			];
     				minimumInNeighbors = Min[sizesInSet];
     				positionOfMinimumInSet = Position[sizesInSet, minimumInNeighbors];
     
     				If[Length[positionOfMinimumInSet] >= 1,
     	 				If[Length[positionOfMinimumInSet] > 1,index = currentSet[[RandomInteger[{1, Length[positionOfMinimumInSet]}]]],index = currentSet[[positionOfMinimumInSet[[1]][[1]]]]],
      					fullElementsList = Table[i, {i, 1, Length[chromosomeA]}];
      					missing = Complement[fullElementsList, final];
      					index = missing[[RandomInteger[{1, Length[missing]}]]
        				]
      				];
     				final = Append[final, index]
     			,{i, 1, max - 1}][[max - 1]];
  				{individual, individual}
  			]
CrossoverChromosomesUniformArithmetic[chromosomeA_,chromosomeB_,alpha_,probability_]:=
		Module[{randomNumber, tempPair},
			tempPair = {chromosomeA,chromosomeB};
			Table[
				randomNumber = RandomReal[{0,1}];
				tempPair = If[randomNumber<=probability, MeanAllelesCrossover[tempPair[[1]],tempPair[[2]],alpha,i], tempPair]
			,{i,1,Length[chromosomeA]}];
			tempPair	
		]
CrossoverChromosomesUniformIntegerArithmetic[chromosomeA_,chromosomeB_,alpha_,probability_]:=
		Module[{randomNumber, tempPair},
			tempPair = {chromosomeA,chromosomeB};
			Table[
				randomNumber = RandomReal[{0,1}];
				tempPair = If[randomNumber<=probability, Round[MeanAllelesCrossover[tempPair[[1]],tempPair[[2]],alpha,i]], tempPair]
			,{i,1,Length[chromosomeA]}];
			tempPair
		] 
CrossoverPopulationOnePoint[population_,selectedIndexes_,probability_] := 
	Module[{randomNumber, j, k, descendance,times,even},
  		randomNumber = RandomReal[{0, 1}];
  		j = -1;
  		k = 0;
  		If[Mod[Length[selectedIndexes],2]==0,even=True,even=False];
  		If[even==True,times=Length[selectedIndexes]/2,times=(Length[selectedIndexes]-1)/2+1];
  		descendance = Table[
  			If[k>=Length[selectedIndexes]-1,k=-1];
    		j = j + 2; k = k + 2;
   			If[randomNumber <= probability,
     			CrossoverChromosomesOnePoint[population[[selectedIndexes[[j]]]],population[[selectedIndexes[[k]]]]],
     			{population[[selectedIndexes[[j]]]],population[[selectedIndexes[[k]]]]}
      		]
    	,{i, 1, times}];
    	If[even==True,Flatten[descendance, 1],Drop[Flatten[descendance, 1],-1]]
  	]
CrossoverPopulationUniform[population_, selectedIndexes_, probability_, uniformityProbability_] :=
 	Module[{randomRealsChromosome, realProbability, crossChromosome, matingPool, newIndividuals, pairedMatingPool, even},
  		even = EvenQ[Length[selectedIndexes]];
		matingPool = RandomSample[population[[selectedIndexes]]];
		matingPool = If[even, matingPool, Append[matingPool, matingPool[[1]]]];
  		pairedMatingPool = Partition[matingPool, 2];
 
  		randomRealsChromosome = RandomReal[{0, 1}, Length[pairedMatingPool]];
  		realProbability = 1 - probability;
  		crossChromosome = Ceiling[Threshold[randomRealsChromosome, realProbability]];
  
  		newIndividuals = Flatten[
				Nest[(Apply[CrossoverUniformWithAccumulator[#1, #2, #3, #4] &, #]) &,{pairedMatingPool, crossChromosome, uniformityProbability, 1}, Length[pairedMatingPool]][[1]], 		
			1];
  
  		If[!even, Drop[newIndividuals, -1], newIndividuals]
  	]
CrossoverPopulationEdgeRecombination[population_, selectedIndexes_,probability_] := 
	Module[{j, k, descendance, randomNumber,even,times},
  		randomNumber = RandomReal[{0, 1}];
  		j = -1;
  		k = 0;
  		If[Mod[Length[selectedIndexes],2]==0,even=True,even=False];
  		If[even==True,times=Length[selectedIndexes]/2,times=(Length[selectedIndexes]-1)/2+1];
  		descendance = Table[
  			If[k>=Length[selectedIndexes]-1,k=-1];
    		j = j + 2; k = k + 2;
    		If[randomNumber <= probability,
    			CrossoverChromosomesEdgeRecombination[population[[selectedIndexes[[j]]]],population[[selectedIndexes[[k]]]]],
    			{population[[selectedIndexes[[j]]]],population[[selectedIndexes[[k]]]]}]
    	,{i, 1, times}];
		If[even==True,Flatten[descendance, 1],Drop[Flatten[descendance, 1],-1]]
	]
CrossoverPopulationUniformArithmetic[population_,selectedIndexes_,probability_,alpha_,uniformProbability_]:=
	Module[{randomNumber, j, k, descendance,even,times},
  		randomNumber = RandomReal[{0, 1}];
  		j = -1;
  		k = 0;
   		If[Mod[Length[selectedIndexes],2]==0,even=True,even=False];
  		If[even==True,times=Length[selectedIndexes]/2,times=(Length[selectedIndexes]-1)/2+1];
  		descendance = Table[
  			If[k>=Length[selectedIndexes]-1,k=-1];
    		j = j + 2; k = k + 2;
    		If[randomNumber <= probability,
    			CrossoverChromosomesUniformArithmetic[population[[selectedIndexes[[j]]]], population[[selectedIndexes[[k]]]], alpha, uniformProbability],
    			{population[[selectedIndexes[[j]]]],population[[selectedIndexes[[k]]]]}]
    	,{i, 1, times}];
    	If[even==True,Flatten[descendance, 1],Drop[Flatten[descendance, 1],-1]]
  	]
CrossoverPopulationUniformIntegerArithmetic[population_,selectedIndexes_,probability_,alpha_,uniformProbability_]:=
	Module[{randomNumber, j, k, descendance,even,times},
  		randomNumber = RandomReal[{0, 1}];
  		j = -1;
  		k = 0;
   		If[Mod[Length[selectedIndexes],2]==0,even=True,even=False];
  		If[even==True,times=Length[selectedIndexes]/2,times=(Length[selectedIndexes]-1)/2+1];
  		descendance = Table[
  			If[k>=Length[selectedIndexes]-1,k=-1];
    		j = j + 2; k = k + 2;
    		If[randomNumber <= probability,
    			CrossoverChromosomesUniformIntegerArithmetic[population[[selectedIndexes[[j]]]], population[[selectedIndexes[[k]]]], alpha, uniformProbability],
    			{population[[selectedIndexes[[j]]]],population[[selectedIndexes[[k]]]]}]
    	,{i, 1, times}];
    	If[even==True,Flatten[descendance, 1],Drop[Flatten[descendance, 1],-1]]
  	]
CrossoverDifferentialEvolution[originalVector_, mutatedVector_, crossoverConstant_] :=
 	Module[{randomForce, randomList},
  		randomForce = RandomInteger[{1, Length[mutatedVector]}];
  		randomList = RandomReal[{0, 1}, Length[mutatedVector]];
  		Table[
   			If[randomList[[i]] <= crossoverConstant || i == randomForce,
    			mutatedVector[[i]],
    			originalVector[[i]]
   			 ]
   		,{i, 1, Length[randomList]}]
 	]

(*Local Search Functions*)
ChromosomeRandomSteepestSearch[chromosome_, fitnessFunction_, iterations_, mutationProbability_] := 
 	Module[{newIndividuals, fitnessesList},
 		newIndividuals = Table[MutateBinaryChromosomeFlip[chromosome,mutationProbability], {i, 1, iterations}];
 		fitnessesList = Map[fitnessFunction, newIndividuals];
 		If[Max[fitnessesList] > fitnessFunction[chromosome],
  			newIndividuals[[fitnessesList // Ordering[#, -1] &]][[1]],
  			chromosome
  		]
 	]
PopulationRandomSteepestSearch[population_, selectedIndexes_, fitnessFunction_, iterations_, mutationProbability_] := 
 	Module[{newIndividuals, replacementRules},
 		newIndividuals = Map[ChromosomeRandomSteepestSearch[#1, fitnessFunction, iterations, mutationProbability] &, population[[selectedIndexes]]];
 		replacementRules = Table[selectedIndexes[[i]] -> newIndividuals[[i]], {i, 1, Length[selectedIndexes]}];
 		ReplacePart[population, replacementRules]
  	]

(*Simple Genetic Algorithm*)
TrainWithSGA[fitnessFunction_,population_,crossoverProbability_,mutationProbability_,generations_,{reportEveryNPopulations_,savePopulations_}]:=
	Module[{populationSize,fitnessesList,reaping,cyclePopulation,crossedOverPopulation,mutatedPopulation,dataArray,maxIndividualIndex},
		(*Variables Initialization*)
		populationSize=Length[population];
		cyclePopulation = population;
		dataArray={{0},{0},{0}};
		(*Training Cycle Starts*)
		Table[
			fitnessesList= Map[fitnessFunction,cyclePopulation];
			reaping= RouletteParentsSelection[fitnessesList,populationSize];
			crossedOverPopulation=CrossoverPopulationOnePoint[cyclePopulation,reaping,crossoverProbability];
			mutatedPopulation=MutateBinaryPopulationFlip[crossedOverPopulation,mutationProbability];
			cyclePopulation=mutatedPopulation;
			dataArray=AppendGenerationFitnessData[dataArray,fitnessesList];
		,{i,1,generations}];
		(*ReturnValues*)
		maxIndividualIndex=Position[fitnessesList,Max[fitnessesList]];
		{dataArray,cyclePopulation[[maxIndividualIndex[[1]]]]}
	]

(*Plotting Functions*)
PreProcessData[outputData_]:=
	Module[{dataOnly,transposedOutput,processedData,maxList,transposedMaxList,max,meanList,transposedMeanList,mean,minList,transposedMinList,min},
		dataOnly = Table[outputData[[i]][[1]], {i, 1, Length[outputData]}];
		transposedOutput = Transpose[dataOnly];

		(*Individual Lists Processing*)
		processedData = Table[
   			maxList = transposedOutput[[j]];
   			transposedMaxList = Transpose[maxList];
   			max = Table[Max[transposedMaxList[[i]]], {i, 1, Length[transposedMaxList]}];
   
  			meanList = transposedOutput[[j]];
   			transposedMeanList = Transpose[meanList];
   			mean = Table[Mean[transposedMeanList[[i]]], {i, 1, Length[transposedMeanList]}];
   
   			minList = transposedOutput[[j]];
   			transposedMinList = Transpose[minList];
   			min = Table[Min[transposedMinList[[i]]], {i, 1, Length[transposedMinList]}];
   
   			{max, mean, min}
   		,{j, 1, 3}]		
	]
PlotSingleRunGA[outputData_]:=
	Module[{},
		ListPlot[outputData,
			Joined -> True,
			PlotRange -> All,
			PlotStyle->{{Blue},{Green},{Red}}
		]
	]	
PlotIteratedGA[outputData_,OptionsPattern[]]:=
	Module[{preProcessedData},
		preProcessedData=PreProcessData[outputData];
   		ListPlot[Flatten[preProcessedData, 1],
			Joined -> True,
 			PlotStyle -> {
   				{Thickness[.00025],Blue}, {Blue, Thickness[.003]}, {Thickness[.00025],Blue},
   				{Thickness[.00025],Green}, {Green, Thickness[.003]}, {Thickness[.00025],Green},
   				{Thickness[.00025],Red}, {Red, Thickness[.003]}, {Thickness[.00025],Red}
   			},(*
 			Filling -> {
   				7 -> {{9}, Directive[LightRed]},
   				4 -> {{6}, Directive[LightGreen]},
   				1 -> {{3}, Directive[LightBlue]}
   			},*)
 			PlotRange -> All,
 			AxesLabel->{"Generations","Fitness"}
 		]	
	]
Options[PlotIteratedGA] = {Filling -> False};

End[]
EndPackage[]

